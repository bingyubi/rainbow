package com.yxyc.rainbow.common.response;


import com.yxyc.rainbow.common.base.BaseResponse;

/**
*  ┏┓　　　┏┓
*┏┛┻━━━┛┻┓
*┃　　　　　　　┃ 　
*┃　　　━　　　┃
*┃　┳┛　┗┳　┃
*┃　　　　　　　┃
*┃　　　┻　　　┃
*┃　　　　　　　┃
*┗━┓　　　┏━┛
*　　┃　　　┃神兽保佑
*　　┃　　　┃代码无BUG！
*　　┃　　　┗━━━┓
*　　┃　　　　　　　┣┓
*　　┃　　　　　　　┏┛
*　　┗┓┓┏━┳┓┏┛
*　　　┃┫┫　┃┫┫
*　　　┗┻┛　┗┻┛
*
* @description 通用对象返回
* @author Yeoman
* @create 2018-04-28 15:17
**/
public class ObjectRestResponse<T> extends BaseResponse {
    T data;
    boolean rel;

    public boolean isRel() {
        return rel;
    }

    public void setRel(boolean rel) {
        this.rel = rel;
    }


    public ObjectRestResponse rel(boolean rel) {
        this.setRel(rel);
        return this;
    }


    public ObjectRestResponse data(T data) {
        this.setData(data);
        return this;
    }
    
    public ObjectRestResponse message(String message){
    	this.setMessage(message);
    	return this;
    }
    
    public ObjectRestResponse status(int status){
    	this.setStatus(status);
    	return this;
    }
    

	public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
