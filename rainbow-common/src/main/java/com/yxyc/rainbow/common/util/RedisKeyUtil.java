package com.yxyc.rainbow.common.util;

import com.yxyc.rainbow.common.constant.CommonConstants;
import com.yxyc.rainbow.common.constant.RedisKeyConstants;

import java.util.Date;

/**
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @author yeoman
 * @description
 * @create 2018/9/1 15:29
 **/
public class RedisKeyUtil {
    /**
     * @param userId
     * @param expire
     * @return
     */
    public static String buildUserAbleKey(String userId, Date expire) {
        return CommonConstants.REDIS_USER_TOKEN + RedisKeyConstants.USER_ABLE + userId + ":" + expire.getTime();
    }

    /**
     * @param userId
     * @param expire
     * @return
     */
    public static String buildUserDisableKey(String userId, Date expire) {
        return CommonConstants.REDIS_USER_TOKEN + RedisKeyConstants.USER_DISABLE + userId + ":" + expire.getTime();
    }
}
